![new term](https://gitlab.com/wolfiy/dotfiles/-/raw/master/tomori-osx/.screenshots/newterm.png)
# minimal tomori
these are the dotfiles assiociated with the above theme. other rices can be found at the root of this repository.

## wallpaper
Tomori Nao, by [Icyro-kun](https://www.deviantart.com/icyro-kun/art/Tomori-Nao-Minimalist-567242260)

## iterm2
Theme: minimal

Tab bar location: Top

Status bar location: Top

Color scheme: Pnevma
