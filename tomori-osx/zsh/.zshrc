#                 _    __           _                           _
#     __      __ | |  / _|  _   _  ( )  ___         ____  ___  | |__    _ __    ___
#     \ \ /\ / / | | | |_  | | | | |/  / __|       |_  / / __| | '_ \  | '__|  / __|
#      \ V  V /  | | |  _| | |_| |     \__ \    _   / /  \__ \ | | | | | |    | (__
#       \_/\_/   |_| |_|    \__, |     |___/   (_) /___| |___/ |_| |_| |_|     \___|
#                           |___/
#

### PROMPT ###
PROMPT="%B%F{230}%n%f%b%B%F{230}@%f%b%B%F{230}%m%f%b%B %b%B%F{158}%~%f%b %# "

### ALIASES ###
# package managers
alias update='brew update -v'
alias upgrade='brew upgrade -v'

# navigation
alias ..='cd ..'
alias ...='cd ../..'
alias .3='cd ../../..'
alias .4='cd ../../../..'
alias .5='cd ../../../../..'

# exa
alias ls='exa -al --color=always --group-directories-first'
alias la='exa -a --color=always --group-directories-first'
alias ll='exa -l --color=always --group-directories-first'
alias lt='exa -aT --color=always --group-directories-first'
alias l.='exa -a | egrep "^\."i'

# colorize grep
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'

# confirm before overwriting sth
alias cp="cp -i"
alias mv='mv -i'
alias rm='rm -i'

# readable sizes
alias df='df -h'
alias free='free -m'

# git
alias addup='git add -u'
alias addall='git add .'
alias branch='git branch'
alias checkout='git checkout'
alias clone='git clone'
alias commit='git commit -m'
alias fetch='git fetch'
alias pull='git pull origin'
alias push='git push origin'
alias status='git status'
alias tag='git tag'
alias newtag='git tag -a'
alias merge='git merge'
alias jcommit='gitmoji -c'

###### OH MY ZSH ######

# installation
export ZSH="/Users/wolfiy/.oh-my-zsh"

# theme
ZSH_THEME="robbyrussell"

# plugins
plugins=(git)
plugins=(zsh-autosuggestions)
source $ZSH/oh-my-zsh.sh
