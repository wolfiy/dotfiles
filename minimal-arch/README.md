![bspwm, polybar, neofetch, cava](https://gitlab.com/wolfiy/dotfiles/-/raw/master/minimal-arch/.screenshots/minimal-arch_neofetch-cava.png)
# minimal bw rice
these are the dotfiles associated with the above theme. other rices can be found on the root of the repo.

here are a few screenshots

## desktop
here are a few screenshots of some themed programs.

### kitty
![kitty](https://gitlab.com/wolfiy/dotfiles/-/raw/master/minimal-arch/.screenshots/minimal-arch_kitty.png)
![kitty (dark)](https://gitlab.com/wolfiy/dotfiles/-/raw/master/minimal-arch/.screenshots/minimal-arch_desktop-dark.png)</br>
</br>

### vim *(wip)*
![vim](https://gitlab.com/wolfiy/dotfiles/-/raw/master/minimal-arch/.screenshots/minimal-arch_vim.png)</br>
</br>

## startpage
is based on [my prior project](https://gitlab.com/wolfiy/wlfys-minimal-startpage). also has a light mode btw

![enter image description here](https://gitlab.com/wolfiy/dotfiles/-/raw/master/minimal-arch/.screenshots/minimal-arch_startpage.png)

## userstyle
and here are some of my userstyles

### anilist
![profile](https://i.imgur.com/Y6ctLc2.png)![browse tab](https://i.imgur.com/CYUHDJ2.png)
</br>
</br>

### google *(wip)*
![enter image description here](https://i.imgur.com/GF32zyl.png)
</br>
</br>
