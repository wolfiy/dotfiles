#!/bin/bash
# 1080 drawing settings
id="$(xinput list --id-only pointer:'Wacom Bamboo One S Pen stylus')"
xinput map-to-output $id HDMI-0
xsetwacom set $id Area 0 0 14720 8280
