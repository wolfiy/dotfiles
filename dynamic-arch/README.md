![bspwm, polybar, neofetch, cava](https://gitlab.com/wolfiy/dotfiles/-/raw/master/dynamic-arch/.screenshots/dynamic-arch_cli.png)
# minimal bw rice
these are the dotfiles associated with the above theme. other rices can be found on the root of the repo.

here are a few screenshots

## desktop
here are a few screenshots of some themed programs.

### kitty
![kitty](https://gitlab.com/wolfiy/dotfiles/-/raw/master/dynamic-arch/.screenshots/dynamic-arch_kitty.png)
</br>

### vim *(wip)*
![vim](https://gitlab.com/wolfiy/dotfiles/-/raw/master/dynamic-arch/.screenshots/dynamic-arch_vim.png)</br>
</br>

## startpage
is based on [my prior project](https://gitlab.com/wolfiy/wlfys-minimal-startpage). also has a light mode btw

![enter image description here](https://gitlab.com/wolfiy/dotfiles/-/raw/master/dynamic-arch/.screenshots/dynamic-arch_startpage.png)
