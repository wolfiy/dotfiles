#!/bin/bash
# 21:9 drawing settings
id="$(xinput list --id-only pointer:'Wacom Bamboo One S Pen stylus')"
xsetwacom get $id Area
xinput map-to-output $id DP-2
xsetwacom set $id Area 0 0 14720 6162
xsetwacom set "Wacom Bamboo One S Pen stylus" Button 2 "pan"
xsetwacom set "Wacom Bamboo One S Pen stylus" "PanScrollThreshold" 300
