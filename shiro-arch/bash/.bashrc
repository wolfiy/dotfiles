#               _  __       _        _               _              
#     __      _| |/ _|_   _( )__    | |__   __ _ ___| |__  _ __ ___ 
#     \ \ /\ / / | |_| | | |/ __|   | '_ \ / _` / __| '_ \| '__/ __|
#      \ V  V /| |  _| |_| |\__ \  _| |_) | (_| \__ \ | | | | | (__ 
#       \_/\_/ |_|_|  \__, ||___/ (_)_.__/ \__,_|___/_| |_|_|  \___|
#                     |___/     
#
# shiro bashrc

### DEFAULT STUFF ###

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
PS1='[\u@\h \W]\$ '

### ARCHIVE EXTRACTION ###
# Made by DistroTube
# usage: ex <file>
ex()
{
  if [ -f $1 ] ; then
    case $1 in
       *.tar.bz2)	tar xjf $1	;;
       *.tar.gz)	tar xzf $1	;;
       *.bz2)		bunzip2 $1	;;
       *.rar)		unrar x $1	;;
       *.gz)		gunzip $1	;;
       *.tar)		tar xf $1	;;
       *.tbz2)		tar xjf $1	;;
       *.tgz)		tar xzf $1	;;
       *.zip)		unzip $1	;;
       *.Z)		uncompress $1	;;
       *.7z)		7z x $1		;;
       *.deb)		ar x $1		;;
       *.tar.xz)	tar xf $1	;;
       *.tar.zst)	unzstd $1	;;
       *)		echo "'$1' cannot be extracted via ex()" ;;
     esac
  else
    echo "'$1' is not a valid file"
  fi 
}


## ALIASES ###
# navigation
alias ..='cd ..'
alias ...='cd ../..'
alias .3='cd ../../..'
alias .4='cd ../../../..'
alias .5='cd ../../../../..'

# pacman and yay
alias pacsyu='sudo pacman -Syyu'			# update only standard pkgs
alias yaysua='yay -Sua --noconfirm'			# update only AUR pkgs
alias yaysyu='yay -Syu --noconfirm'			# update standard and AUR pkgs
alias unlock='sudo rm /var/lib/pacman/db.lck'		# remove pacman lock

# changing "ls" to "exa"
alias ls='exa -al --color=always --group-directories-first'	
alias la='exa -a --color=always --group-directories-first'	# all files and dirs
alias ll='exa -l --color=always --group-directories-first'	# long format
alias lt='exa -aT --color=always --group-directories-first'	# tree listing
alias l.='exa -a | egrep "^\."'

# colorize grep output (good for log files)
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'

# confirm before overwriting something
alias cp="cp -i"
alias mv='mv -i'
alias rm='rm -i'

# adding flags
alias df='df -h'	# human-readable sizes
alias free='free -m'	# show sizes in MB

# git
alias addup='git add -u'
alias addall='git add .'
alias branch='git branch'
alias checkout='git checkout'
alias clone='git clone'
alias commit='git commit -m'
alias fetch='git fetch'
alias pull='git pull origin'
alias push='git push origin'
alias status='git status'
alias tag='git tag'
alias newtag='git tag -a'

# shutdown or reboot
alias ssn="sudo shutdown now"
alias sr="sudo reboot"
alias ss="systemctl suspend"

# forgetting sudo
alias please='sudo !!'

### RICE ###
# Import colorscheme from 'wal' asynchronously
# &   # Run the process in the background.
# ( ) # Hide shell job control messages.
(cat ~/.cache/wal/sequences &)

# Alternative (blocks terminal for 0-3ms)
cat ~/.cache/wal/sequences

# To add support for TTYs this line can be optionally added.
source ~/.cache/wal/colors-tty.sh

# kitty completion
source <(kitty + complete setup bash)
